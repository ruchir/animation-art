//! Animation-Art
//!
//! Ruchir Elukurthy 2022
//! About: This project uses nannou framework which is a creative coding framework in Rust. This project uses this framwork
//! to create an animation art design. The code for this app uses and MVC pattern.

//! Reference 1: https://www.local-guru.net/blog/2020/12/24/nannou-experiment---particles
//! Reference 2: https://www.youtube.com/watch?v=d9lsT4kJo44&list=PLwtLEJr-BkXbscQZLpvCvgYMtkoTCCDyR&index=4
//! Reference 3: https://github.com/nannou-org/nannou

use nannou::noise::*;
use nannou::prelude::*;
use std::io;

const NUM_CIRCLES: usize = 2000;

fn main() {
    nannou::app(model).update(update).simple_window(view).run();
}

//Template for drawing circles with position and velocity.
struct Circle {
    positions: Vec2,
    velocity: Vec2,
}

//Function new and update to initialize and update the location of circles.
impl Circle {
    fn new(x: f32, y: f32) -> Self {
        Circle {
            positions: vec2(x, y),
            velocity: vec2(0.0, 0.0),
        }
    }

    fn update(&mut self, dir: Vec2) {
        self.positions += self.velocity;
        self.velocity += dir;
    }
}

//This is used for initializing a vertex of a point.
struct Vertex {
    position: Point2,
}

//Two points are used to initilize a location for a point.
struct Line {
    point1: Vertex,
    point2: Vertex,
}

//Drawing a line between the specified two points.
impl Line {
    fn display(&self, draw: &Draw) {
        draw.line()
            .points(self.point1.position, self.point2.position)
            .color(RED);
    }
}

//Using the line implementation above, U is drawn here.
struct DrawU {
    draw_lines: Vec<Line>,
}

//Function to draw U.
impl DrawU {
    fn drawing_u(&mut self) {
        let straight_line_one = Line {
            point1: Vertex {
                position: pt2(-20.0, 0.0),
            },
            point2: Vertex {
                position: pt2(-20.0, 150.0),
            },
        };
        let straight_line_two = Line {
            point1: Vertex {
                position: pt2(10.0, 0.0),
            },
            point2: Vertex {
                position: pt2(10.0, 150.0),
            },
        };
        let horizontal_line = Line {
            point1: Vertex {
                position: pt2(-20.0, 0.0),
            },
            point2: Vertex {
                position: pt2(10.0, 0.0),
            },
        };
        self.draw_lines.push(straight_line_one);
        self.draw_lines.push(straight_line_two);
        self.draw_lines.push(horizontal_line);
    }
}

//Using the line implementation above, S is drawn here.
struct DrawS {
    draw_lines: Vec<Line>,
}

//Functions to draw S.
impl DrawS {
    fn drawing_s(&mut self) {
        let tilt_line_one = Line {
            point1: Vertex {
                position: pt2(80.0, 150.0),
            },
            point2: Vertex {
                position: pt2(40.0, 100.0),
            },
        };
        let tilt_line_two = Line {
            point1: Vertex {
                position: pt2(40.0, 100.0),
            },
            point2: Vertex {
                position: pt2(80.0, 70.0),
            },
        };
        let tilt_line_three = Line {
            point1: Vertex {
                position: pt2(80.0, 70.0),
            },
            point2: Vertex {
                position: pt2(40.0, 0.0),
            },
        };
        self.draw_lines.push(tilt_line_one);
        self.draw_lines.push(tilt_line_two);
        self.draw_lines.push(tilt_line_three);
    }
}

//This is the model for this app that has the collection of drawings ,circles, and colors.
struct Model {
    drawing_u: DrawU,
    drawing_s: DrawS,
    time_with_one_color: f64,
    circles: Vec<Circle>,
    radius: f32,
    red: f32,
    blue: f32,
    green: f32,
}

//This is the implementation of the model where we define
impl Model {
    fn display(&self, drawing: &Draw) {
        //This is used to draw an R.
        let mut point1 = pt2(-100.0, 150.0);
        let mut point2 = pt2(-110.0, 150.0);
        let mut point3 = pt2(-110.0, 0.0);
        let mut point4 = pt2(-100.0, 0.0);

        //Involves a quad to draw a verticle line.
        drawing
            .quad()
            .color(STEELBLUE)
            .points(point1, point2, point3, point4);

        point1 = pt2(-40.0, 0.0);
        point2 = pt2(-45.0, 5.0);
        point3 = pt2(-100.0, 85.0);
        point4 = pt2(-110.0, 80.0);

        drawing
            .quad()
            .color(STEELBLUE)
            .points(point1, point2, point3, point4);

        point1 = pt2(-110.0, 70.0);
        point2 = pt2(-60.0, 100.0);
        point3 = pt2(-110.0, 160.0);

        //A triangle for the for R.
        drawing
            .tri()
            .color(STEELBLUE)
            .points(point1, point2, point3);

        for line in &self.drawing_u.draw_lines {
            line.display(drawing);
        }

        for line in &self.drawing_s.draw_lines {
            line.display(drawing);
        }

        point1 = pt2(100.0, 140.0);
        point2 = pt2(100.0, 150.0);
        point3 = pt2(160.0, 150.0);
        point4 = pt2(160.0, 140.0);

        drawing
            .quad()
            .color(STEELBLUE)
            .points(point1, point2, point3, point4);

        point1 = pt2(125.0, 140.0);
        point2 = pt2(135.0, 140.0);
        point3 = pt2(135.0, 0.0);
        point4 = pt2(125.0, 0.0);

        drawing
            .quad()
            .color(STEELBLUE)
            .points(point1, point2, point3, point4);
    }
}

//This is the implementation for the model consists function calls to all the implementation functions.
fn model(app: &App) -> Model {
    app.new_window();

    let mut lines_u = DrawU {
        draw_lines: Vec::new(),
    };
    let mut lines_s = DrawS {
        draw_lines: Vec::new(),
    };

    lines_u.drawing_u();
    lines_s.drawing_s();

    let mut circles = Vec::new();
    for _i in 0..NUM_CIRCLES {
        let thing = Circle::new(
            (random::<f32>() - 0.5) * 1024.0,
            (random::<f32>() - 0.5) * 1024.0,
        );
        circles.push(thing);
    }

    Model {
        drawing_u: lines_u,
        drawing_s: lines_s,
        time_with_one_color: get_time_for_color_change(),
        circles,
        radius: radius_for_circles(),
        red: get_red_for_circles(),
        blue: get_blue_for_circles(),
        green: get_green_for_circles(),
    }
}

//This is used to update the location of circles.
fn update(app: &App, model: &mut Model, _update: Update) {
    let noise = nannou::noise::Perlin::new();
    let time = (app.elapsed_frames() as f64 / 100.) * 0.05;
    for i in 0..NUM_CIRCLES {
        let position = &mut model.circles[i];
        let x = noise.get([
            position.positions.x as f64,
            position.positions.y as f64,
            time as f64,
        ]) as f32;
        let y = noise.get([
            -position.positions.y as f64,
            position.positions.x as f64,
            time as f64,
        ]) as f32;
        position.update(vec2(x, y));
    }
}

//This is the view for the app which is used to draw the items.
fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    let t = app.elapsed_frames() as f64 / 100.;
    frame.clear(BLACK);

    let curr_time = t % (model.time_with_one_color * 2.0);

    //switch colors between blue and black for the background.
    if curr_time > 0.0 && curr_time < model.time_with_one_color {
        frame.clear(BLUE);
    }
    if curr_time > model.time_with_one_color && curr_time < model.time_with_one_color * 2.0 {
        frame.clear(BLACK);
    }

    if app.elapsed_frames() == 1 {
        draw.background().color(BLACK);
    }

    for thing in model.circles.iter() {
        draw.ellipse()
            .xy(thing.positions)
            .radius(model.radius)
            .color(rgb(model.red, model.blue, model.green));
    }

    model.display(&draw);

    draw.to_frame(app, &frame).unwrap();
}

//Function for user input time for background screen color change.
fn get_time_for_color_change() -> f64 {
    println!("Time change 1.0 = 3 seconds approximately");
    println!("Enter a time for screen change (between 0.1 and 1000.0)");
    let mut number = String::new();
    io::stdin()
        .read_line(&mut number)
        .expect("Failed to read input");
    let time = number.trim().parse().expect("Not a valid number");
    if time <= 0.0 {
        error();
    }
    time
}

//Function for user entered radius for circles.
fn radius_for_circles() -> f32 {
    println!("Enter the radius of circles for screen (between 0.1 and 100.0)");
    let mut radius = String::new();
    io::stdin()
        .read_line(&mut radius)
        .expect("Failed to read input");
    let float_radius = radius.trim().parse().expect("Not a valid number");
    if float_radius <= 0.0 {
        error();
    }
    float_radius
}

//Function for adding red color value for rgb for the circles.
fn get_red_for_circles() -> f32 {
    println!("Enter the red value for color of circles (between 0.0 to 1.0)");
    let mut red = String::new();
    io::stdin()
        .read_line(&mut red)
        .expect("Failed to read input");
    let red_color_value = red.trim().parse().expect("Not a valid number");
    if !(0.0..=1.0).contains(&red_color_value) {
        error();
    }
    red_color_value
}

//Function for adding blue color value for rgb for the circles.
fn get_blue_for_circles() -> f32 {
    println!("Enter the blue value for color of circles (between 0.0 to 1.0)");
    let mut blue = String::new();
    io::stdin()
        .read_line(&mut blue)
        .expect("Failed to read input");
    let blue_color_value = blue.trim().parse().expect("Not a valid number");
    if !(0.0..=1.0).contains(&blue_color_value) {
        error();
    }
    blue_color_value
}

//Function for adding green color value for rgb for the circles.
fn get_green_for_circles() -> f32 {
    println!("Enter the blue value for color of circles (between 0.0 to 1.0)");
    let mut green = String::new();
    io::stdin()
        .read_line(&mut green)
        .expect("Failed to read input");
    let green_color_value = green.trim().parse().expect("Not a valid number");
    if !(0.0..=1.0).contains(&green_color_value) {
        error();
    }
    green_color_value
}

//Function for errors.
fn error() -> ! {
    eprintln!("Error with value entered");
    std::process::exit(1);
}

//These are the tests for the code.
#[test]
fn test_animation_art() {
    let v = get_time_for_color_change();
    assert!((0.1..1000.01).contains(&v));
    let radius = radius_for_circles();
    assert!((0.1..100.01).contains(&radius));
    let red = get_red_for_circles();
    assert!((0.0..1.01).contains(&red));
    let blue = get_blue_for_circles();
    assert!((0.0..1.01).contains(&blue));
    let green = get_green_for_circles();
    assert!((0.0..1.01).contains(&green));
}
