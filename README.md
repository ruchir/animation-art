# Animation Art

##About
*
This project uses nannou framework which is a creative coding framework in Rust. This project uses this framwork to create an animation art design. The code for this app uses and MVC pattern. Also, includes a command line interface select sizes and colors for screens for users.
*

##Dependencies
*
https://docs.rs/nannou/latest/nannou/
*

##Build and Run instructions
*
Build and run using cargo: **cargo run --release**

Then, enter the command line information.
*

##Test instructions
*
Run test using cargo: **cargo test -- --nocapture**
*

##Development Milestone:
*
* [x]Understand nannou framework
* [x]Decide if additional libaries are going to be needed
* [x]Template on how things in the should look like
* [x]Template for drawing lines.
* [x]Template for building shapes.
* [x]Template for animations.
* [x]Finish implementation.
* [x]Getting user inputs for color and sizes.
* [x]Creating tests.
* [x]Finishing with project and updating readme.
*

##How Things Went
*
There was a steep learning curve for nannou, especially understanding MVC pattern in nannou Rust. Drawing lines and shapes was easier once the template for them was in place. Passing user entered data was the most tricky part and changing color of screen based on time. Overall, things went well, I was able to have the animations and the implementation I wanted my project to have.
*

